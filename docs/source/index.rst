.. Modular Quantum Chemistry Project documentation master file, created by
   sphinx-quickstart on Sun Jun 13 09:41:10 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to Modular Quantum Chemistry Project's documentation!
==============================================================
This site is dedicated to the development and application of 
modular computational platform for the general science and 
technology community. As the old-fashioned approach to developing 
and maintaining computational programs becomes obsolete, 
an emerging concept of software modularity offers an 
elegant and timely solution of the looming problems by providing 
an open development ecosystem where new computational approaches 
can be rapidly created from modules uploaded at the web 
repository by the interested users and developers. 
The first implementation of such an environment 
is already available in the form of a 
web-based platform at MQCP_.

.. _MQCP: https://mqcp.edison.re.kr

.. toctree::
   :maxdepth: 2
   :caption: Introduction

   intro

.. toctree::
   :maxdepth: 2
   :caption: For contributors

   for_contributors
   datatypes

.. toctree::
   :maxdepth: 2
   :caption: For users

   modules
   workflows
   mrsf
   reks

.. toctree::
   :maxdepth: 2
   :caption: Quantum Chemistry in Action

   qc_in_action
