The List of MQCP Datatypes
--------------------------

Datatypes specify the data format of inputs and outputs of modules, which are files in nature.

.. _Input file:

Input file (``inp``)
....................

This datatype is used to set up runtime parameters for modules using
human-readable format. It has a following format::

    TITLE
    MOLECULE DESCRIPTION SECTION
    PARAMETERS SECTION

The title is a single line describing the calculation. It's not used by modules.

In the ``MOLECULE DESCRIPTION SECTION`` the number of atoms, the atomic types
and their coordinates are given. It resembles the chemical ``.xyz`` file format.
First the number of atoms is given and then every atom is specified by its
nucleic charge (``Q``) and Cartesian coordinates (``X``, ``Y`` and ``Z``) in
Angstrom::

    NATOM=<number of atoms>
    Q    X    Y    Z
    ...
    Q    X    Y    Z

In the parameters section various options for modules are specified in the format ``OPTION=VALUE``. Only one option per line is allowed.

Here is an example of input file::

  C3H8 molecule
  natom=11
   6.0  -0.2814116433  -0.0914064155   0.0569146050
   6.0  -0.0108420173   1.3451955038  -0.4302553427
   6.0  -1.5164903822  -0.7099985842  -0.6138764961
   1.0   0.5852250728  -0.7142140575  -0.1449089075
   1.0  -0.3980187758  -0.0846805453   1.1288285918
   1.0   0.2402967369   1.3696657263  -1.4851529300
   1.0  -0.9033547396   1.9429289671  -0.2719580118
   1.0   0.8176724909   1.7961170180   0.1140239055
   1.0  -1.9541584238  -1.5030395028  -0.0171310512
   1.0  -1.2780973834  -1.1336105793  -1.5855662126
   1.0  -2.2852454650   0.0377688194  -0.7774286105
  charge=0
  scftype=rhf

.. _Simulation metadata:

Simulation metadata (``inf``)
..............................

This datatype contains common parameters of simulation which is shared among modules.
It can be read and written as an unformatted Fortran 90 structure.

The corresponding Fortran 90 type is defined as follows:

.. code:: fortran

  type inf
      integer             :: natom      !< The number of atom
      integer             :: charge     !< Molecular charge
      integer             :: hamilton   !< The method of calculations,
                                        !< 1= EHT, 2=CNDO/2, 3=INDO, 10=HF, 20=DFT
      integer             :: scftype    !< Reference wavefunction,
                                        !< 1= RHF 2= UHF 3= ROHF
      integer             :: nelec      !< The number of electron
      integer             :: nelec_A    !< The number of alpha electron
      integer             :: nelec_B    !< The number of beta electron
      integer             :: mult       !< Spin multiplicity
      integer             :: nvelec     !< The number of valence electron
      integer             :: nocc       !< The number of occupied orbitals
                                        !< nOCC = nelec/2 for RHF
                                        !< nOCC = nelec_A for ROHF/UHF with mult=3
                                        !< nOCC = nelec/2 for ROHF/UHF with mult=1
      integer             :: maxit      !< The maximum number of iterations
      real(REAL64)        :: conv       !< Convergency criteria of SCF
      integer             :: idamp      !< Damp the density if idamp=1
      integer             :: mulliken   !< Print out Mulliken population charge if mulliken=1
      integer             :: nbasis     !< The number of basis set functions
      integer             :: n2basis    !< n2basis : nbasis*(nbasis+1)/2
      integer             :: l1         !< Used for compatibility, L1 = nbasis
      integer             :: l2         !< Used for compatibility, L2 = nbasis * (nbasis+1)/2 = n2basis
      integer             :: l3         !< Used for compatibility, L3 = nbasis * nbasis
      real(REAL64)        :: energy     !< Total energy
      real(REAL64)        :: enuc       !< Nuclear repulsion energy
      integer             :: nalpha     !<  Number of alpha electrons
      integer             :: nbeta      !<  Number of beta electrons
      character(len=20)   :: &
          basis_name = ''               !< The basis set name for ab initio method
      integer             :: npfunc     !< The number of p functions
      integer             :: mem        !< Available memory size in Mega Byte
      integer             :: runtype    !< Run type 
                                        !<  1 - energy, 2 - gradient, 3 - geometry optimization
      integer             :: geomit     !< Maximum number of geometry optimization iteration
      integer             :: guess      !< Not used now
      real(REAL64)        :: xdamp      !< Not used now
      real(REAL64)        :: psinrm     !< Wavefunction normalization
      real(REAL64)        :: ehf1       !< One-electron energy
      real(REAL64)        :: vee        !< Two-electron energy
      real(REAL64)        :: nenergy    !< Nuclear repulsion energy
      real(REAL64)        :: etot       !< Total energy
      real(REAL64)        :: vne        !< Nucleus-electron potential energy
      real(REAL64)        :: vnn        !< Nucleus-nucleus potential energy
      real(REAL64)        :: vtot       !< Total potential energy
      real(REAL64)        :: tkin       !< Total kinetic energy
      real(REAL64)        :: virial     !< Virial ratio (v/t)
      real(REAL64)        :: olde       !< For geometry optimizations, energy of the previous step
      real(REAL64)        :: depre      !< For geometry optimizations, predicted energy change
      integer(I2B)        :: nolds      !< For geometry optimizations, number of steps performed
      integer             :: acell      !< Not used now
      real(REAL64)        :: ebot       !< Bottom bound of energy range in band calculations
      real(REAL64)        :: etop       !< Upper bound of energy range in band calculations
      real(REAL64)        :: &
          dfttyp(20)                    !< Parameters of XC functional
      character(len=1024) :: &
          XC_functional_name = ''       !< Name of the XC functional
      integer             :: &
          periodic_dim
  end type

.. todo::

    Replace with generic config format

.. _Atomic information:

Atomic information (``xyz``)
............................

Describes molecular structure.

The ``xyz`` file contains the total number of atoms as well as the atomic information.

The formatting of the mqcp.xyz file format is as follows:

::

    Natom=<N:=number of atoms>
    ATOM(1)
    ATOM(2)
    ...
    ATOM(N)

The ATOM type is defined as follows

.. code-block:: fortran

   TYPE atom
      INTEGER(INT32) :: &
 !        The number of atomic basis set
          basis_n, &
 !        The highest principal quantum number, n
          n_max, &
 !        The real high principal quantum number, n:w
          nc_atm, &
 !        The number of core electrons
          izcore
      REAL(REAL64) :: &
 !        Atomic number or nuclear charge
          zn, &
 !        Number of valence electrons
          ezn, &
 !        Cartesian coordinates
          coord(3), &
 !        The highest zeta
          zet_atm, &
 !        Atomic mass
          mass, &
 !        Gradient
          De(3)
      CHARACTER(LEN=2) :: &
 !        Atomic symbol
          Symbol
   END TYPE atom

.. _Atomic basis set:

Atomic basis set (``bas``)
............................

Provides basis set library.

This type of data is used to pass basis set library to the MQCP program.  The
library format is basically GAMESS(US)-style with minor adjustments.  The basis
set file contains several basis set entries for different elements separated by
blank line.  Symbols ``!``, ``$``, ``#``, and ``&`` are recognized as in-line
comment symbols and the rest of the line will be ignored. Comments can appear
anywhere in the file.
    
::

    ! this is a comment
    ATOM BASIS ENTRY
    <blank line>
    ATOM BASIS ENTRY
    <blank line>
    ATOM BASIS ENTRY
    <blank line>

.. note::
    The last blank line is *mandatory*.

Atom basis set entry starts from the line containing the element name or its
chemical symbol (e.g. ``CARBON`` or ``C``).  Next, one or more shell entries
appears. They must not be separated by blank lines. The atom basis entry format::

    <Element name | symbol>
    SHELL ENTRY
    SHELL ENTRY
    ...
    SHELL ENTRY

The shell entry contains information of the shell type, the contraction degree,
and all exponents and contraction coefficients for every primitive Gaussian in
the shell. Shell type denotes the angular momentum and is given as a letter S,
P, D, F, G, H, or I, corresponding to angular momentum 0, 1, 2, 3, 4, 5, and 6
respectively.

.. warning::
    In contrast to GAMESS(US) basis set format, Pople's ``SP`` (or ``L``)
    shell types are not supported. ``S`` and ``P`` components of these shells
    should be present as separate entries.
    
After the header, representing the shell type and contraction degree, the
parameters of primitive Gaussians are give one at a line. Each primitive gaussian
statement contains integer sequence number (which is ignored and is present only
for compatibility), and two double precision numbers corresponding to the
exponential and contraction coefficients respectively.  Hereby, the shell entry
format is as follows::

    <shell type := {S,P,D,F,G,H,I}>      <N, contraction degree>
    1        exponent 1       contraction coefficient 1
    ...
    k        exponent k       contraction coefficient k
    ...
    N        exponent N       contraction coefficient N


Here is an example of the 6-31G++ basis set entry for the carbon::

    CARBON
    S   6
    1         0.3047524880E+04       0.1834737132E-02
    2         0.4573695180E+03       0.1403732281E-01
    3         0.1039486850E+03       0.6884262226E-01
    4         0.2921015530E+02       0.2321844432E+00
    5         0.9286662960E+01       0.4679413484E+00
    6         0.3163926960E+01       0.3623119853E+00
    S   3
    1         0.7868272350E+01      -0.1193324198E+00
    2         0.1881288540E+01      -0.1608541517E+00
    3         0.5442492580E+00       0.1143456438E+01
    S   1
    1         0.1687144782E+00       0.1000000000E+01
    S   1
    1         0.4380000000E-01       0.1000000000E+01
    P   3
    1         0.7868272350E+01       0.6899906659E-01
    2         0.1881288540E+01       0.3164239610E+00
    3         0.5442492580E+00       0.7443082909E+00
    P   1
    1         0.1687144782E+00       0.1000000000E+01
    P   1
    1         0.4380000000E-01       0.1000000000E+01

The set pre-formatted basis sets is available :download:`here
<assets/basis_sets.tgz>`.  All provided basis sets can be also downloaded from
the `Basis Set Exchange website <https://www.basissetexchange.org/>`_.
    
.. _One-electron integrals:

One-electron integrals (``hst``)
................................

In the ``hst`` data type the matrices of one-electron integrals are stored.
They are namely:

- Core hamiltonian matrix (|Hcore|)
- Matrix of overlap integrals (|ovlp|)
- Matrix of electronic kinetic energy integrals (|KEI|)

The core hamiltonian matrix comprises both electron-nuclei Coulomb attraction
integrals (|Ven|) and electronic kinetic energy integrals and is virtually a sum
of |KEI| and |Ven| matrices:

.. math::
    H_\mathrm{core} = V^\mathrm{en} + T

|Hcore|, |ovlp|, and |KEI| are symmetric (:math:`N\times N`) matrices. They are represented in the
`packed format <https://www.netlib.org/lapack/lug/node123.html>`_ where only an
upper (``U``) triangle of a matrix is stored.  Matrix dimensions :math:`N` is equal to
the number of basis set function for the given molecule. In the output, these
three matrices are written as a plain text sequentially one after another using
Fortran 90 unformatted output.  The order, in which matrices are written is
precisely |Hcore|, then |ovlp|, and finally |KEI|.

.. |Hcore| replace:: :math:`H_\mathrm{core}`
.. |Ven| replace:: :math:`V^\mathrm{en}`
.. |ovlp| replace:: :math:`S`
.. |KEI| replace:: :math:`T`

.. _Wavefunction:

Wavefunction (``den``)
......................

This datatype contains information about the molecular orbitals and electronic
density for :math:`\alpha` and optionally :math:`\beta` spin. It includes the
following components in order:

- :math:`\alpha`-spin density matrix (|Da|);
- matrix |Q| of orthonormal molecular orbitals constructed from the atomic basis set;
- :math:`\alpha`-spin molecular orbitals;
- eigenvalues of the :math:`\alpha`-spin molecular orbitals;
- (optional) :math:`\beta`-spin density matrix;
- (optional) :math:`\beta`-spin molecular orbitals

|Da| and |Db| density are symmetric :math:`N\times N` matrices and are stored in a
`packed format <https://www.netlib.org/lapack/lug/node123.html>`_ (upper
triangle).
Here, :math:`N` denotes the number of basis set functions.  :math:`\alpha` and
:math:`\beta` molecular orbitals, as well as matrix |Q| are :math:`N\times N`
matrices which are stored in the `full square format
<https://www.netlib.org/lapack/lug/node122.html>`_.  :math:`\alpha`-orbital
energies is a vector of dimension :math:`N`.
All the data is written using the Fortran 90 unformatted I/O.

.. |Da| replace:: :math:`D_\alpha`
.. |Db| replace:: :math:`D_\beta`
.. |Q| replace:: :math:`Q`

.. _Shells data:

Shells data (``shl``)
.....................

This datatype contains a structured shell data for a given molecule. It can be read and written as an unformatted Fortran 90 structure.

The corresponding Fortran 90 type is defined as follows:

.. code-block:: fortran

   TYPE shell_structure

      REAL(REAL64), DIMENSION(MXGTOT) :: &
 !      Exponential coefficients for shells
        EX, &
 !      Contraction coefficients for shells
        CS, CP, CD, CF, CG, CH, CI

      INTEGER(INT32), DIMENSION(MXSH) :: &
 !      Location of the first exponent and the first
 !      contraction coefficient contained in a particular shell
        KSTART, &
 !      Atomic center indices
        KATOM, &
 !      Shell types, 1,2,3,4,5,6,7 for S,P,D,F,G,H,I respectively
        KTYPE, &
 !      Degrees of shell contraction
        KNG, &
 !      Indices of first AO of the shell in the basis set
        KLOC, &
 !      Starting and ending indices of the shell
 !                   S    P    D    F   G   H   I   L
 !            KMIN   1    2    5   11  21  36  57   1
 !            KMAX   1    4   10   20  35  56  84   4
        KMIN, KMAX

 !     Normalization constant
       REAL(REAL64) :: PNRM(84)

 !     AO symbol
       CHARACTER(LEN=8) :: BFLAB(MXAO)
   END TYPE shell_structure

.. _Log output:

Log output (``log``)
....................

This datatype is a simple text-based log file which contain status report of all
modules. It has no specific format.
