The List of MQCP Modules
------------------------
Modules on MQCP need to specify inputs and outputs, which correspond to the
arguments of functions or subroutines. Technically, inputs and outputs are the
reading/writing of files within modules.

read_molecule
.............

This module should be called prior to calling any other module. It prepares the
calculation by filling up the MQCP internal data structures: simulation metadata
and molecular structure. The human-readable input file provided by the user must
contain molecular coordinates and all relevant input parameters.

This module has one input and three output ports.

Inputs:

1. :ref:`inp <INput file>` - MQCP input file

Outputs:

1. :ref:`xyz <Atomic information>` - molecular structure
2. :ref:`inf <Simulation metadata>` - MQCP simulation metadata
3. :ref:`log <Log output>` - log file

apply_basis
...........

The purpose of this module is to create molecular basis set from the file with
molecule structure (atomic coordinates and elements) and basis set library.

This module has four input and four output ports.

Inputs:

1. :ref:`xyz <Atomic information>` - molecular structure
2. :ref:`inf <Simulation metadata>` -  MQCP simulation metadata
3. :ref:`bas <Atomic basis set>` - basis set library
4. :ref:`log <Log output>` - log file

Outputs:

1. :ref:`xyz <Atomic information>` - updated molecular structure
2. :ref:`inf <Simulation metadata>` - updated MQCP simulation metadata
3. :ref:`shl <Shells data>` - molecular basis set
4. :ref:`log <Log output>` - updated log file

hsandt
......

This module computes the following classes of one-electron integrals:

- Atomic basis set overlap integrals (|ovlp|)
- Electron-nuclei Coulomb attraction integrals (|Ven|)
- Electronic kinetic energy integrals (|KEI|)

These integrals are stored as matrices and saved into the :ref:`hst format
<One-electron integrals>` file. Matrix elements are computed as follows.

Overlap integrals:

.. math::
    S_{ij} = \braket{ \phi_i | \phi_j}

Electronic kinetic energy integrals matrix elements:

.. math::
    T_{ij} = -\frac{1}{2} \braket{ \phi_i | \nabla^2 | \phi_j}

Electron-nuclei Coulomb attraction matrix elements:

.. math::
    V^\mathrm{en}_{ij} = \sum_{n \in \mathrm{nuc}} \braket{ \phi_i | \frac{Q_n}{|r-R_n|} | \phi_j}

Here, :math:`\phi_i` denotes :math:`i`-th basis set function, :math:`Q_n` and
:math:`R_n` - are charge and coordinates of :math:`n`-th nuclei, :math:`r` - is
electronic coordinate.

This module has four input and two output ports:

Inputs:

1. :ref:`xyz <Atomic information>` - molecular structure
2. :ref:`inf <Simulation metadata>` -  MQCP simulation metadata
3. :ref:`shl <Shells data>` - molecular basis set
4. :ref:`log <Log output>` - log file 

Outputs:

1. :ref:`hst <One-electron integrals>` - one-electron integral matrices
2. :ref:`log <Log output>` - updated log file 

.. |Hcore| replace:: :math:`H_\mathrm{core}`
.. |Ven| replace:: :math:`V^\mathrm{en}`
.. |ovlp| replace:: :math:`S`
.. |KEI| replace:: :math:`T`

guess_huckel
............

This module computes the initial guess to the density matrix using the
semiempirical extended Huckel method. This method uses Huzinaga's MINI basis
set. The resulting wavefunction is projected on the molecular basis set provided
as an input. This method is applicable to the elements from hydrogen up to radon.

This module has five input and three output ports:

Inputs:

1. :ref:`xyz <Atomic information>` - molecular structure
2. :ref:`inf <Simulation metadata>` -  MQCP simulation metadata
3. :ref:`shl <Shells data>` - molecular basis set
4. :ref:`hst <One-electron integrals>` - one-electron integral matrices
5. :ref:`log <Log output>` - log file 

Outputs:

1. :ref:`dmt <Wavefunction>` - density matrix
2. :ref:`log <Log output>` - updated log file 
3. :ref:`hst <One-electron integrals>` - one-electron integral matrices

guess_hcore
...........

This module computes the initial guess to the density matrix by diagonalization of the one-electron Hamiltonian. This method is applicable to the all elements.

This module has five input and three output ports:

Inputs:

1. :ref:`xyz <Atomic information>` - molecular structure
2. :ref:`inf <Simulation metadata>` -  MQCP simulation metadata
3. :ref:`shl <Shells data>` - molecular basis set
4. :ref:`hst <One-electron integrals>` - one-electron integral matrices
5. :ref:`log <Log output>` - log file 

Outputs:

1. :ref:`dmt <Wavefunction>` - density matrix
2. :ref:`log <Log output>` - updated log file 
3. :ref:`hst <One-electron integrals>` - one-electron integral matrices


rhf_energy
..........

This module computes the electronic energy and molecular orbitals using
closed-shell variant of the Hartree-Fock method. The module uses direct
calculation of the two-electron integrals tensor and applies Schwartz inequality
to screen out small values of integrals. The self-consistent field equations of
the Hartree-Fock method are solved with the help of DIIS method. The module
needs an initial guess density matrix for running.

This module has six input and three output ports:

Inputs:

1. :ref:`xyz <Atomic information>` - molecular structure
2. :ref:`inf <Simulation metadata>` -  MQCP simulation metadata
3. :ref:`shl <Shells data>` - molecular basis set
4. :ref:`hst <One-electron integrals>` - one-electron integral matrices
5. :ref:`dmt <Wavefunction>` - initial guess density matrix
6. :ref:`log <Log output>` - log file 

Outputs:

1. :ref:`moe <Wavefunction>` - resulting molecular orbitals and energy
2. :ref:`inf <Simulation metadata>` - updated MQCP simulation metadata
3. :ref:`log <Log output>` - updated log file 

rhf_gradient
............

This module computes the electronic energy gradient using closed-shell variant
of the Hartree-Fock method. It takes the wavefunction of the converged
Hartree-Fock calculations as an input, which can be obtained using
``rhf_energy`` :ref:`module <rhf_energy>`.

This module has six input and two output ports:

Inputs:

1. :ref:`xyz <Atomic information>` - molecular structure
2. :ref:`inf <Simulation metadata>` -  MQCP simulation metadata
3. :ref:`shl <Shells data>` - molecular basis set
4. :ref:`hst <One-electron integrals>` - one-electron integral matrices
5. :ref:`moe <Wavefunction>` - the converged Hartree-Fock wavefunction
6. :ref:`log <Log output>` - log file

Outputs:

1. :ref:`xyz <Atomic information>` - updated molecular data which contains atomic gradients
2. :ref:`log <Log output>` - updated log file


dft_rhf_energy
..............

This module computes the electronic energy and molecular orbitals using
closed-shell variant of the density functional theory (DFT) method. The module
uses direct calculation of the two-electron integrals tensor and applies
Schwartz inequality to screen out small values of integrals. The self-consistent
field equations are solved with the help of DIIS method. The module needs an
initial guess density matrix for running. Grid-DFT contribution is computed with
the help of the LibXC library for DFT functionals.

This module has six input and three output ports:

Inputs:

1. :ref:`xyz <Atomic information>` - molecular structure
2. :ref:`inf <Simulation metadata>` -  MQCP simulation metadata
3. :ref:`shl <Shells data>` - molecular basis set
4. :ref:`hst <One-electron integrals>` - one-electron integral matrices
5. :ref:`dmt <Wavefunction>` - initial guess density matrix
6. :ref:`log <Log output>` - log file 

Outputs:

1. :ref:`moe <Wavefunction>` - resulting molecular orbitals and energy
2. :ref:`inf <Simulation metadata>` - updated MQCP simulation metadata
3. :ref:`log <Log output>` - updated log file 

dft_rhf_gradient
................

This module computes the electronic energy gradients using closed-shell variant
of the density functional theory (DFT) method. It takes the density of the
converged Kohn-Sham DFT calculation, which can be obtained using
``dft_rhf_energy`` :ref:`module <dft_rhf_energy>`. Grid-DFT derivative
contribution is computed with the help of the LibXC library for DFT functionals.

This module has six input and three output ports:

Inputs:

1. :ref:`xyz <Atomic information>` - molecular structure
2. :ref:`inf <Simulation metadata>` -  MQCP simulation metadata
3. :ref:`shl <Shells data>` - molecular basis set
4. :ref:`hst <One-electron integrals>` - one-electron integral matrices
5. :ref:`moe <Wavefunction>` - the converged DFT density
6. :ref:`log <Log output>` - log file

Outputs:

1. :ref:`xyz <Atomic information>` - updated molecular data which contains atomic gradients
2. :ref:`log <Log output>` - updated log file

dft_urohf_energy
................

This module computes the electronic energy and molecular orbitals using
unrestricted or restricted open-shell density functional theory (DFT) method.
The module uses direct calculation of the two-electron integrals tensor and
applies Schwartz inequality to screen out small values of integrals. The
self-consistent field equations are solved with the help of DIIS method. The
module needs an initial guess density matrix for running. Grid-DFT contribution
is computed with the help of the LibXC library for DFT functionals.

This module has six input and three output ports:

Inputs:

1. :ref:`xyz <Atomic information>` - molecular structure
2. :ref:`inf <Simulation metadata>` -  MQCP simulation metadata
3. :ref:`shl <Shells data>` - molecular basis set
4. :ref:`hst <One-electron integrals>` - one-electron integral matrices
5. :ref:`dmt <Wavefunction>` - initial guess density matrix
6. :ref:`log <Log output>` - log file 

Outputs:

1. :ref:`moe <Wavefunction>` - resulting molecular orbitals and energy
2. :ref:`inf <Simulation metadata>` - updated MQCP simulation metadata
3. :ref:`log <Log output>` - updated log file 

dft_urohf_gradient
..................

This module computes the electronic energy gradients using unrestricted or
restricted open-shell density functional theory (DFT) method. It takes the
density of the converged Kohn-Sham DFT calculation, which can be obtained using
``dft_urohf_energy`` :ref:`module <dft_urohf_energy>`. Grid-DFT derivative
contribution is computed with the help of the LibXC library for DFT functionals.

This module has six input and three output ports:

Inputs:

1. :ref:`xyz <Atomic information>` - molecular structure
2. :ref:`inf <Simulation metadata>` -  MQCP simulation metadata
3. :ref:`shl <Shells data>` - molecular basis set
4. :ref:`hst <One-electron integrals>` - one-electron integral matrices
5. :ref:`moe <Wavefunction>` - the converged DFT density
6. :ref:`log <Log output>` - log file

Outputs:

1. :ref:`xyz <Atomic information>` - updated molecular data which contains atomic gradients
2. :ref:`log <Log output>` - updated log file

polm_band
.........

This module runs band structure computations. Bands calculations make sense if
the molecular structure is an ordered oligomer with insignificant deviations
from periodic regularity.

To carry out such calculations, restrictions are introduced on the order of
atoms in the initial structure:

1. periodically repeated units are given one by one from the one end of the oligomer to the other;
2. the order of atoms in each unit mast be identical;
3. terminal groups of atoms (normally terminal H atoms) must be numbered last.

The keyword ``ATOM_CELL`` must be given in the MQCP input
file to define the number if atom in the unit cell. Optional keywords
``EN_LOWER`` and ``EN_UPPER`` can be specified in the input file to define
desired energy range (in eV). The input example is the following::

 The input example of [C2H2]15 oligomer.
 natom=62
 6.0      0.305450260       -17.455528492         0.000000000
 1.0      1.375447159       -17.458106142         0.000000000
 6.0     -0.386647810       -16.250082413         0.000000000
 1.0     -1.456644709       -16.247504763         0.000000000
 6.0      0.311250090       -15.047984853         0.000000000
 ...
 6.0      0.386647810        16.250082413         0.000000000
 1.0      1.456644709        16.247504763         0.000000000
 6.0     -0.305450260        17.455528492         0.000000000
 1.0     -1.375447159        17.458106142         0.000000000
 1.0      0.231780490        18.380884162         0.000000000
 1.0     -0.231780490       -18.380884162         0.000000000
 charge=0
 Hamilton= dft
 Basis=6-31G(d)
 charge=0
 scftype=rhf
 runtype=energy
 atoms_cell=4
 en_lower=-12
 en_upper=5
 $libxc functional=b3lyp5 $end

This module has six input and one output ports:

Inputs:

1. :ref:`xyz <Atomic information>` - molecular structure
2. :ref:`inf <Simulation metadata>` -  MQCP simulation metadata
3. :ref:`shl <Shells data>` - molecular basis set
4. :ref:`hst <One-electron integrals>` - one-electron integral matrices
5. :ref:`moe <Wavefunction>` - the converged orbitals
6. :ref:`log <Log output>` - log file 

Outputs:

1. :ref:`log <Log output>` - updated log file 