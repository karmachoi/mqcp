What is MQCP?
-------------
MQCP stands for Modular Quantum Chemistry Project.

What does MQCP try to accomplish?
---------------------------------
MQCP is initiated to promote modularization of scientific software for

- improving interoperability
- facilitating collaborations
- reducing steep learning curve 
- reducing maintenance overheads
- adapting to the future heterogeneous HW
- most of all, eliminating redundant code developments over and over again...

How to accomplish it?
---------------------
Contrasting to the old-style packaging approach, 
where various methods and theories are integrated into a single software, MQCP
proposes multiple software of modules.

What is Module?
------------------
The modules are standalone executables in linux environment as shown below. Each module needs at least one input (blue square below) and output(red square) ports, which are basically file IO ports for data in/out.

.. figure:: images/module.png
   :width: 30%
   :align: center

   An example of a module: extended Huckel method for initial orbital guess

Why MQCP is so exciting?
------------------------
The real power of MQCP lies at the flexibility of creating **workflows** by connecting Modules. More specifically, the output of **Module A** can be directly connected to the input of **Module B** as shown below. The only restriction to this is they (input and output) should have the same datatypes for a given connection. The possible combinations grow infinitely as the number of Modules increases! 

.. figure:: images/workflow.png
   :width: 80%
   :align: center

   An example of a workflow: R-B3LYP calculation

How to create Modules?
------------------------
Two projects are undergoing.

Open Quantum Project
....................
Demonstrative quantum mechanical software with the concept of modularization is being
developed, which shall be utilized as a template for additional module developments.
See more `here <http://www.openqp.org>`_. 

The functionalities of OQP are

- RHF, ROHF and UHF
- KS-DFT
- MRSF-TDDFT
- REKS
- Energy and Gradient

Data IO Library
.................
In order to allow the interconnections of modules in the form of workflow, clear
and exact definitions of data are essential. A library for this is under development.

How can I use/contribute?
-------------------------
Please sign up on `MQCP website <https://mqcp.edison.re.kr>`_. 
You can start using uploaded Modules or uploading your Modules right away!

Who are we?
-----------
We are quantum chemistry group at Kyungpook National University, 
developing new quantum chemistry theories of MRSF-TDDFT and REKS.  
